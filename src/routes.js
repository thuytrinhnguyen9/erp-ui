import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import AuthLayout from './layouts/Auth';
import ErrorLayout from './layouts/Error';
import DashboardLayout from './layouts/Dashboard';
import Dashboards from './views/Dashboards';
import PrivateRoute from './privateRoute';

export default [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/dashboards"/>
  },
  {
    path: '/auth',
    component: AuthLayout,
    routes: [
      {
        path: '/auth/login',
        exact: true,
        component: lazy(() => import('src/views/Login'))
      },
      {
        path: '/auth/register',
        exact: true,
        component: lazy(() => import('src/views/Register'))
      },
      {
        component: () => <Redirect to="/errors/error-404"/>
      }
    ]
  },
  {
    path: '/errors',
    component: ErrorLayout,
    routes: [
      {
        path: '/errors/error-401',
        exact: true,
        component: lazy(() => import('src/views/Error401'))
      },
      {
        path: '/errors/error-404',
        exact: true,
        component: lazy(() => import('src/views/Error404'))
      },
      {
        path: '/errors/error-500',
        exact: true,
        component: lazy(() => import('src/views/Error500'))
      },
      {
        component: () => <Redirect to="/errors/error-404"/>
      }
    ]
  },
  {
    route: '*',
    component: (props) => <PrivateRoute component={DashboardLayout} {...props}/>,
    routes: [
      {
        path: '/dashboards',
        exact: true,
        component: Dashboards
      },
      {
        path: '/management/customers',
        exact: true,
        component: lazy(() => import('src/views/CustomerList'))
      },
      {
        path: '/management/projects',
        exact: true,
        component: lazy(() => import('src/views/ProjectList'))
      },
      {
        path: '/management/products',
        exact: true,
        component: lazy(() => import('src/views/ProductList'))
      },
      {
        component: () => <Redirect to="/errors/error-404"/>
      }
    ]
  }
];
