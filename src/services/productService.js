import axios from '../utils/axios';
import { handleResponse } from '../helpers/handleResponse';
import { handleError } from '../helpers/handleError';

export const productService = {
  findAll,
  save,
  _delete
};

function findAll() {
  return axios.get('/products/')
    .catch(handleError)
    .then(handleResponse);
}

function save(name, price, description) {
  return axios.post('/products/',
    { name, price, description })
    .catch(handleError)
    .then(handleResponse);
}

function _delete(selectedProducts) {
  return axios.delete('/products/', { data: selectedProducts })
    .catch(handleError)
    .then(handleResponse)
    ;
}
