import axios from '../utils/axios';
import { handleResponse } from '../helpers/handleResponse';
import { handleError } from '../helpers/handleError';
import { authService } from './authService';

export const customerService = {
  findAll,
  save,
  _delete
};

function findAll() {
  return axios.get('/customers/')
    .catch(handleError)
    .then(handleResponse);
}

function save(firstName, lastName, location, type) {
  return axios.post('/customers/',
    { firstName, lastName, location, type })
    .catch(handleError)
    .then(handleResponse);
}

function _delete(selectedCustomers) {
  return axios.delete('/customers/', { data: selectedCustomers })
    .catch(handleError)
    .then(handleResponse);
}
