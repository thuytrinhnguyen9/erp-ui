import { authService } from './authService';
import axios from '../utils/axios';
import { handleResponse } from '../helpers/handleResponse';
import { handleError } from '../helpers/handleError';

export const projectService = {
  findAll,
  save,
  _delete
};

function findAll() {
  return axios.get('/projects/')
    .catch(handleError)
    .then(handleResponse);
}

function save(name, duration, status) {
  return axios.post('/projects/',
    { name, duration, status })
    .catch(handleError)
    .then(handleResponse);
}

function _delete(selectedProjects) {
  return axios.delete('/projects/', { data: selectedProjects })
    .catch(handleError)
    .then(handleResponse)
    ;
}

