import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import PropTypes, { number } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { TextField } from '@material-ui/core';
import { useForm } from 'react-hook-form';

function CreateProductModal({ open, onCancel, onSave }) {
  const {register, errors, handleSubmit} = useForm();
  const handleSave = (data) => {
    const {name, price, description} = data;
    onSave(name, price, description);
  };

  return (
    <div>
      <Dialog open={open} maxWidth="sm" aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add product</DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item md={6}>
              <TextField
                error={Boolean(errors.name)}
                helperText={errors.name ? errors.name.message : null}
                fullWidth
                label="Product Name"
                name="name"
                variant="outlined"
                inputRef={
                  register({
                    required: 'Product name is required'
                    }
                  )
                }
              />
            </Grid>
            <Grid item md={6}>
              <TextField
                error={Boolean(errors.price)}
                helperText={errors.price ? errors.price.message : null}
                fullWidth
                label="Product Price"
                name="price"
                variant="outlined"
                type="number"
                inputRef={
                  register({
                      required: 'Product name is required'
                    }
                  )
                }
              />
            </Grid>
            <Grid item md={6}>
              <TextField
                error={Boolean(errors.description)}
                helperText={errors.description ? errors.description.message : null}
                fullWidth
                label="Product Description"
                name="description"
                variant="outlined"
                inputRef={
                  register({
                      required: 'Product name is required'
                    }
                  )
                }
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              onCancel();
            }}
            color="primary">
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit(handleSave)}>
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

CreateProductModal.propTypes = {
  open: PropTypes.bool,
  onCancel: PropTypes.func,
  onSave: PropTypes.func
};

export default CreateProductModal;
