import React, { useEffect, useState } from 'react';
import {
  Button,
  Card, CardContent, CardHeader, Grid, Table, TableBody, TableCell, TableHead, TableRow
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TableEditBar from '../../components/TableEditBar';
import DeleteIcon from '@material-ui/icons/DeleteOutline';

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

function Results({ products, className, onDelete }) {
  const classes = useStyles();
  const [selectedProducts, setSelectedProducts] = useState([]);

  useEffect(() => {
    const idList = products.map((product) => product.id);
    setSelectedProducts(selectedProducts.filter((p) => idList.includes(p)));
  }, [products]);

  const handleSelectAll = (event) => {
    if (event.target.checked) {
      setSelectedProducts(products.map((p) => p.id));
    } else {
      setSelectedProducts([]);
    }
  };

  const handleSelectSingle = (event, id) => {
    if (event.target.checked) {
      setSelectedProducts([...selectedProducts, id]);
    } else {
      setSelectedProducts(selectedProducts.filter((c) => c !== id));
    }
  };

  const handleDelete = () => {
    onDelete(selectedProducts);
  };
  return (
    <div className={clsx(classes.root, className)}>
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {products.length}
        {' '}
        Records found.
      </Typography>
      <Card>
        <CardHeader title="All Products"/>
        <CardContent className={classes.content}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <Checkbox
                    indeterminate={selectedProducts.length !== 0 && selectedProducts.length !== products.length}
                    color="primary"
                    onChange={(event) => handleSelectAll(event)}
                    checked={selectedProducts.length === products.length}
                  />
                </TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Description</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {products.map((p) => (
                <TableRow key={p.id}>
                  <TableCell>
                    <Checkbox
                      color="primary"
                      checked={selectedProducts.includes(p.id)}
                      onChange={(event) => handleSelectSingle(event, p.id)}
                    />
                  </TableCell>
                  <TableCell>{p.name}</TableCell>
                  <TableCell>{p.price}</TableCell>
                  <TableCell>{p.description}</TableCell>
                  <TableCell>
                    <Button onClick={() => onDelete([p.id])}>
                      <DeleteIcon />
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </CardContent>
      </Card>
      <TableEditBar selected={selectedProducts} onDelete={handleDelete}/>
    </div>
  );
}

Results.propTypes = {
  products: PropTypes.array,
  className: PropTypes.string,
  onDelete: PropTypes.func
};

export default Results;
