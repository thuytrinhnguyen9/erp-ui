import React, { useEffect, useState } from 'react';
import axios from 'src/utils/axios';
import { makeStyles } from '@material-ui/styles';
import { Container } from '@material-ui/core';
import Results from './Results';
import Header from './Header';
import Page from '../../components/Page';
import CreateProductModal from './CreateProductModal';
import SuccessSnackbar from '../../components/SuccessSnackbar';
import DeleteConfirmationDialog from '../../components/DeleteConfirmationDialog';
import { productService } from '../../services/productService';
import { authService } from '../../services/authService';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  header: {
    marginBottom: theme.spacing(3)
  },
  filter: {
    marginTop: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(6)
  }
}));

function ProductList() {
  const classes = useStyles();
  const [createDialogOpen, setCreateDialogOpen] = useState(false);
  const [products, setProducts] = useState([]);
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const [successSnackbarOpen, setSuccessSnackbarOpen] = useState(false);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [message, setMessage] = useState(null);

  const loadData = () => {
    productService
      .findAll()
      .then(products => setProducts(products));
  };

  useEffect(() => {
    loadData();
  }, []);

  const handleSave = (name, price, description) => {
    productService.save(name, price, description)
      .then(() => {
        setCreateDialogOpen(false);
        loadData();
        setSuccessSnackbarOpen(true);
        setMessage('Product saved successfully!');
      });
  };

  const handleDelete = (selectedProducts) => {
    setDeleteConfirmOpen(true);
    setSelectedProducts(selectedProducts);
  };

  const handleDeleteConfirmed = () => {
   productService._delete(selectedProducts)
      .then(() => {
        loadData();
        setSuccessSnackbarOpen(true);
        setMessage(`Product${selectedProducts.length > 1 ? 's' : ''} deleted successfully!`);
        setDeleteConfirmOpen(false);
      });
  };

  return (
    <Page
      className={classes.root}
      title="Product List"
    >
      <Container maxWidth={false}>
        <Header onAdd={() => setCreateDialogOpen(true)}/>
        <Results
          products={products}
          className={classes.results}
          onDelete={handleDelete}
        />
      </Container>
      <CreateProductModal
        open={createDialogOpen}
        onCancel={() => setCreateDialogOpen(false)}
        onSave={handleSave}
      />
      <SuccessSnackbar
        message={message}
        open={successSnackbarOpen}
        onClose={() => setSuccessSnackbarOpen(false)}
      />
      <DeleteConfirmationDialog
        open={deleteConfirmOpen}
        onCancel={() => setDeleteConfirmOpen(false)}
        onOk={handleDeleteConfirmed}
      />
    </Page>
  );
}

export default ProductList;
