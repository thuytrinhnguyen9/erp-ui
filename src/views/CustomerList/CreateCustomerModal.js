import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { TextField } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { register } from '../../serviceWorker';
import { useForm } from 'react-hook-form';

function CreateCustomerModal({ open, onCancel, onSave }) {
  const { register, errors, handleSubmit } = useForm();

  const handleSave = (data) => {
    const { firstName, lastName, location, type } = data;
    onSave(firstName, lastName, location, type);
  };


  return (
    <Dialog open={open} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Add Customer</DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.firstName)}
              helperText={errors.firstName ? errors.firstName.message : null}
              fullWidth
              label="First Name"
              name="firstName"
              variant="outlined"
              inputRef={
                register({
                  required: 'First name is required',
                  minLength: {
                    value: 2,
                    message: 'First name must have at least 2 characters'
                  }
                })
              }
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.lastName)}
              helperText={errors.lastName ? errors.lastName.message : null}
              fullWidth
              label="Last Name"
              name="lastName"
              variant="outlined"
              inputRef={
                register({
                  required: 'Last name is required',
                  minLength: {
                    value: 2,
                    message: 'Last name must have at least 2 characters'
                  }
                })}
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.location)}
              helperText={errors.location ? errors.location.message : null}
              fullWidth
              label="City"
              name="location"
              variant="outlined"
              inputRef={
                register({
                  required: 'City is required'
                })
              }
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.type)}
              helperText={errors.type ? errors.type.message : null}
              fullWidth
              label="Type"
              name="type"
              variant="outlined"
              inputRef={
                register({
                  required: 'Type is required'
                })
              }
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onCancel}>
          Cancel
        </Button>
        <Button
          color="primary"
          variant="contained"
          onClick={handleSubmit(handleSave)}
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

CreateCustomerModal.propTypes = {
  open: PropTypes.bool,
  onCancel: PropTypes.func,
  onSave: PropTypes.func
};

export default CreateCustomerModal;
