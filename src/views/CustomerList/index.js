import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container } from '@material-ui/core';
import axios from 'src/utils/axios';
import Page from 'src/components/Page';
import SearchBar from 'src/components/SearchBar';
import Header from './Header';
import Results from './Results';
import CreateCustomerModal from './CreateCustomerModal';
import SuccessSnackbar from '../../components/SuccessSnackbar';
import DeleteConfirmationDialog from '../../components/DeleteConfirmationDialog';
import { customerService } from '../../services/customerService';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

function CustomerManagementList() {
  const classes = useStyles();
  const [customers, setCustomers] = useState([]);
  const [selectedCustomers, setSelectedCustomers] = useState([]);
  const [createDialogOpen, setCreateDialogOpen] = useState(false);
  const [createSuccessSnackbarOpen, setCreateSuccessSnackbarOpen] = useState(false);
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const [message, setMessage] = useState(null);

  const loadData = () => {
    customerService
      .findAll().then(data => {
      setCustomers(data);
    });
  };

  const handleSave = (firstName, lastName, location, type) => {
      customerService.save(firstName, lastName, location, type)
      .then(() => {
        setCreateDialogOpen(false);
        loadData();
        setCreateSuccessSnackbarOpen(true);
        setMessage('Customer saved successfully!');
      });
  };

  const handleDelete = (selectedCustomers) => {
    setSelectedCustomers(selectedCustomers);
    setDeleteConfirmOpen(true);
  };

  const handleDeleteConfirmed = () => {
    customerService._delete(selectedCustomers)
      .then(() => {
        loadData();
        setDeleteConfirmOpen(false);
        setCreateSuccessSnackbarOpen(true);
        setMessage(`Customer${selectedCustomers.length > 1 ? 's' : ' '} deleted successfully!`);
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <Page
      className={classes.root}
      title="Customer Management List"
    >
      <Container maxWidth={false}>
        <Header onOpen={() => {
          setCreateDialogOpen(true);
        }}
        />
        <SearchBar
        />
        {customers && (
          <Results
            className={classes.results}
            customers={customers}
            onDelete={handleDelete}
          />
        )}
      </Container>
      <CreateCustomerModal
        open={createDialogOpen}
        onCancel={() => setCreateDialogOpen(false)}
        onSave={handleSave}
      />
      <SuccessSnackbar
        message={message}
        open={createSuccessSnackbarOpen}
        onClose={() => setCreateSuccessSnackbarOpen(false)}/>
      <DeleteConfirmationDialog open={deleteConfirmOpen} onOk={handleDeleteConfirmed}
                                onCancel={() => setDeleteConfirmOpen(false)}/>
    </Page>
  );
}

export default CustomerManagementList;
