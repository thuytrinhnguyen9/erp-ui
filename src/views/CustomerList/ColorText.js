import React from 'react';
import PropTypes from 'prop-types';

function ColorText({ text, color }) {
  return (
    <div style={{ color }}>
      {text}
    </div>
  );
}

ColorText.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string
};

export default ColorText;
