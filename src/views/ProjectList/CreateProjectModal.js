import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { TextField } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { useForm } from 'react-hook-form';

const useStyles = makeStyles((theme) => ({
  policy: {
    display: 'flex',
    alignItems: 'center'
  }
}));

function CreateProjectModal({ open, onCancel, onSave }) {
  const classes = useStyles();
  const { register, errors, handleSubmit } = useForm();

  const handleSave = (data) => {
    const { name, duration, status } = data;
    onSave(name, duration, status);
  };

  return (
    <Dialog open={open} maxWidth="sm" fullWidth aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Add project</DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.name)}
              helperText={errors.name ? errors.name.message : null}
              fullWidth
              label="Project Name"
              name="name"
              variant="outlined"
              inputRef={register({ required: 'Project name is required' })}
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.duration)}
              helperText={errors.duration ? errors.duration.message : null}
              fullWidth
              label="Project Duration (days)"
              name="duration"
              variant="outlined"
              type="number"
              inputRef={register({
                required: 'Project duration is required',
                min: { value: 1, message: 'Project duration must be positive' }
              })}
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              error={Boolean(errors.status)}
              helperText={errors.status ? errors.status.message : null}
              fullWidth
              label="Project Status"
              name="status"
              variant="outlined"
              inputRef={register({ required: 'Project status is required' })}
            />
          </Grid>
          <Grid item>
            <div className={classes.policy}>
              <Checkbox value="uncontrolled" inputProps={{ 'aria-label': 'uncontrolled-checkbox' }}/>
              <Typography
                color="textSecondary"
                variant="body1">
                Everything I say, I mean it!
              </Typography>
            </div>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onCancel}>
          Cancel
        </Button>
        <Button
          color="primary"
          variant="contained"
          onClick={handleSubmit(handleSave)}
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

CreateProjectModal.propTypes = {
  open: PropTypes.bool,
  onCancel: PropTypes.func,
  onSave: PropTypes.func
};

export default CreateProjectModal;
