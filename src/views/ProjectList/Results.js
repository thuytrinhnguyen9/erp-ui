import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import GenericMoreButton from 'src/components/GenericMoreButton';
import TableEditBar from '../../components/TableEditBar';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/DeleteOutline';

const useStyles = makeStyles((theme) => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

function Results({ className, projects, onDelete, ...rest }) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selectedProjects, setSelectedProjects] = useState([]);

  useEffect(() => {
    const idList = projects.map((project) => project.id);
    setSelectedProjects(selectedProjects.filter((p) => idList.includes(p)));
  }, [projects]);

  const handleChangePage = (event, page) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(event.target.value);
  };

  const handleSelectAll = (event) => {
    if (event.target.checked) {
      setSelectedProjects(projects.map((p) => p.id));
    } else {
      setSelectedProjects([]);
    }
  };

  const handleSelectSingle = (event, id) => {
    if (event.target.checked) {
      setSelectedProjects([...selectedProjects, id]);
    } else {
      setSelectedProjects(selectedProjects.filter(p => p !== id));
    }
  };

  const handleDelete = () => {
    onDelete(selectedProjects);
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {projects.length}
        {' '}
        Records found. Page
        {' '}
        {page + 1}
        {' '}
        of
        {' '}
        {Math.ceil(projects.length / rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader
          action={<GenericMoreButton/>}
          title="All projects"
        />
        <Divider/>
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <Checkbox
                        color="primary"
                        onChange={handleSelectAll}
                        indeterminate={selectedProjects.length !== projects.length && selectedProjects.length !== 0}
                        checked={selectedProjects.length === projects.length}
                      />
                    </TableCell>
                    <TableCell>Project Name</TableCell>
                    <TableCell>Duration (days)</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {projects.map((project) => (
                    <TableRow
                      hover
                      key={project.id}
                    >
                      <TableCell>
                        <Checkbox
                          color="primary"
                          checked={selectedProjects.includes(project.id)}
                          onChange={(event) => handleSelectSingle(event, project.id)}
                        />
                      </TableCell>
                      <TableCell>{project.name}</TableCell>
                      <TableCell>{project.duration}</TableCell>
                      <TableCell>{project.status}</TableCell>
                      <TableCell>
                        <Button onClick={() => onDelete([project.id])}>
                          <DeleteIcon />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={projects.length}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
      <TableEditBar selected={selectedProjects} onDelete={handleDelete}/>
    </div>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  projects: PropTypes.array,
  onDelete: PropTypes.func
};

Results.defaultProps = {
  projects: []
};

export default Results;
