import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import axios from 'src/utils/axios';
import Page from 'src/components/Page';
import { Container } from '@material-ui/core';
import Header from './Header';
import Results from './Results';
import CreateProjectModal from './CreateProjectModal';
import SuccessSnackbar from '../../components/SuccessSnackbar';
import DeleteConfirmationDialog from '../../components/DeleteConfirmationDialog';
import { projectService } from '../../services/projectService';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

function ProjectManagementList() {
  const classes = useStyles();
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [createSuccessSnackbarOpen, setCreateSuccessSnackbarOpen] = useState(false);
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const [selectedProjects, setSelectedProjects] = useState([]);
  const [projects, setProjects] = useState([]);
  const [message, setMessage] = useState(null);

  const loadData = () => {
    projectService.findAll()
      .then(projects => setProjects(projects));
  };

  useEffect(() => {
    loadData();
  }, []);

  const handleSave = (name, duration, status) => {
    projectService.save(name, duration, status)
      .then(() => {
        setCreateModalOpen(false);
        loadData();
        setCreateSuccessSnackbarOpen(true);
        setMessage('Project saved successfully!');
      });
  };

  const handleDelete = (selectedProjects) => {
    setDeleteConfirmOpen(true);
    setSelectedProjects(selectedProjects);
  };

  const handleDeleteConfirmed = () => {
    projectService._delete(selectedProjects)
      .then(() => {
        loadData();
        setCreateSuccessSnackbarOpen(true);
        setMessage(`Project${selectedProjects.length > 1 ? 's' : ' '} deleted successfully!`);
        setDeleteConfirmOpen(false);
      });
  };

  return (
    <Page
      className={classes.root}
      title="Project Management List"
    >
      <Container maxWidth={false}>
        <Header onOpen={() => {
          setCreateModalOpen(true);
        }}
        />
        <Results
          className={classes.results}
          projects={projects}
          onDelete={handleDelete}
        />
      </Container>
      <CreateProjectModal
        open={createModalOpen}
        onCancel={() => setCreateModalOpen(false)}
        onSave={handleSave}
      />
      <SuccessSnackbar
        message={message}
        open={createSuccessSnackbarOpen}
        onClose={() => setCreateSuccessSnackbarOpen(false)}
      />
      <DeleteConfirmationDialog
        open={deleteConfirmOpen}
        onCancel={() => setDeleteConfirmOpen(false)}
        onOk={handleDeleteConfirmed}
      />
    </Page>
  );
}

export default ProjectManagementList;
